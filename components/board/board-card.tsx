import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useState } from 'react'
import styles from '../styles/Home.module.css'
import { Box, Typography, Divider, ListItem, ListItemButton, 
    ListItemText, List, AppBar, Toolbar, IconButton, Button, Drawer 
} from '@mui/material'
import {Menu as MenuIcon} from '@mui/icons-material'

export interface Props {
    from: string
    to: string
    message: any,
    isOdd: boolean
}


const BoardCard: NextPage<Props> = ({ from, to, message, isOdd }) => {

    let boardDegree = isOdd ? '1.2' : '-1.2';
    let textBoardDegree = isOdd ? '-1.2' : '1.2';

    const boardStyle = {
        backgroundColor: '#FFDA33',
        color: 'black',
        padding: '10px',
        border: '1px black solid',
        borderRadius: '10px',
        transform: `scale(1) rotate(${boardDegree}deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg)`
    }

    const textBoardStyle = {
        transform: `scale(1) rotate(${textBoardDegree}deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg)`,
        
    }

    const dynamicContentTypographyStyle = {
        fontFamily: "'Shadows Into Light', cursive",
    }

    const typographyStyle = {
        marginTop: '10px'
    }

    const messageTypoGraphyStyle = {
        fontFamily: "'Shadows Into Light', cursive",
        marginTop: '-5px'
    }

    return (
        <div style={boardStyle}>
            <div style={textBoardStyle}>
                <Typography style={typographyStyle}>Dari:&nbsp;<span style={dynamicContentTypographyStyle}>{from}</span></Typography>
                <Typography style={typographyStyle}>Untuk:&nbsp;<span style={dynamicContentTypographyStyle}>{to}</span></Typography>
                <Typography style={typographyStyle} component={'div'}>
                    Pesan:<br />
                    <div style={messageTypoGraphyStyle} dangerouslySetInnerHTML={{__html:message}} />
                </Typography>
            </div>
        </div>
    )
}

export default BoardCard