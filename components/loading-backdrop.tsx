import { Backdrop, CircularProgress } from "@mui/material";
import { NextPage } from "next";

export interface Props {
    isOpen: boolean
}

const LoadingBackDrop: NextPage<Props> = ({ isOpen = false }) => (
    <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isOpen}
    >
        <CircularProgress color="inherit" />
    </Backdrop>
)

export default LoadingBackDrop;