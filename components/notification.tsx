import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useState } from 'react'
import styles from '../styles/Home.module.css'
import { Snackbar, IconButton
} from '@mui/material'
import {Menu as MenuIcon} from '@mui/icons-material'

export interface Props {
    open: boolean,
    onClose?: any,
    message?: string,
    action?: any
}


const Notification: NextPage<Props> = ({ open = false, onClose = () => {}, message = '', action = () => {} }) => {

    return (
        <>
            <Snackbar
                style={{ marginTop: '10%'}}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                open={open}
                autoHideDuration={3000}
                onClose={onClose}
                message={message}
                action={(
                    <>
                        <IconButton
                            size="small"
                            aria-label="close"
                            color="inherit"
                            onClick={onClose}
                        >
                            X
                        </IconButton>
                    </>
                )}
            />
        </>
    )

}

export default Notification