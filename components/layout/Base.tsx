import type { NextPage } from 'next'
import { useState } from 'react'
import Props from '../../interface/props/LayoutProps'
import { Box, Typography, Divider, ListItem, ListItemButton, 
    ListItemText, List, AppBar, Toolbar, IconButton, Button, Drawer 
} from '@mui/material'
import {Menu as MenuIcon} from '@mui/icons-material'
import Link from 'next/link'

const drawerWidth = 240;
const navItems2 = [
    {
        id: 0,
        label: 'Beranda',
        redirectTo: '/'
    },
    {
        id: 1,
        label: 'Kirim Pesan ke Temanmu',
        redirectTo: '/create'
    },
    {
        id: 2,
        label: 'Tentang Situs Ini',
        redirectTo: '/about-me'
    },
]

const BaseLayout: NextPage<Props> = ({ children }: Props) => {

    const [mobileOpen, setMobileOpen] = useState(false);
    const appVersion = process.env.PUBLIC_NEXT_APP_VERSION || 'development';

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography variant="h6" sx={{ my: 2 }}>
                Mading School-{appVersion}
            </Typography>
            <Divider />
            <List>
                {navItems2.map((item) => (
                    <ListItem key={item.id} disablePadding>
                        <Link href={item.redirectTo}>
                            <ListItemButton sx={{ textAlign: 'left' }}>
                                <ListItemText primary={item.label} />
                            </ListItemButton>
                        </Link>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    return (
        <div>
            <AppBar component="nav">
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                    >
                        Mading School-{appVersion}
                    </Typography>
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                        {navItems2.map((item) => (
                            <Link key={item.id} href={item.redirectTo}>
                                <Button sx={{ color: '#fff' }}>
                                    {item.label}
                                </Button>
                            </Link>
                        ))}
                    </Box>
                </Toolbar>
            </AppBar>
            <Box component="nav">
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                {drawer}
                </Drawer>
            </Box>
            <Box component="main" sx={{ p: 3, m: 2 }}>
                <Toolbar />
                <video autoPlay muted loop style={{position: 'fixed', right: 0, bottom: 0, minWidth: '100%', minHeight: '100%'}}>
                    <source src={process.env.PUBLIC_NEXT_LAYOUT_BACKGROUND} type="video/mp4" />
                </video>
                <Box style={{ position: 'sticky',background: 'rgba(255,255,255, 0.3)', color: '#f1f1f1', width: '98%', padding: '20px'}}>
                    { children }
                </Box>
            </Box>
        </div>   
    )
}

export default BaseLayout
