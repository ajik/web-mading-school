import { ChangeEvent, ReactElement, useEffect, useState } from 'react'
import BaseLayout from '../../components/layout/Base'
import { NextPageWithLayout } from '../_app'
import {Typography} from '@mui/material'
// import constants from '../../utils/constants'
import { Box, Grid, Button, Pagination } from '@mui/material'
import BoardCard from '../../components/board/board-card'
import { Message } from '../../interface/message'
import { useRouter } from 'next/router'
import LoadingBackDrop from '../../components/loading-backdrop'

const ArchiveIndex: NextPageWithLayout = () => {

    const [ messages, setMessages ] = useState<Message[]>([]);
    const [ limit, setLimit ] = useState(1);
    const [ page, setPage ] = useState(1);
    const [ totalPage, setTotalPage ] = useState(1);
    const [ totalData, setTotalData ] = useState(0);
    const [ loading, setLoading ] = useState(false);
    const router = useRouter();

    const headers: HeadersInit = new Headers();
    headers.set('Accept', 'application/json');

    const loadMessages = async (page = 1) => {
        setLoading(true);
        try {
            const data = await fetch(
                `${process.env.PUBLIC_NEXT_BACKEND_API}/board?limit=${limit}&page=${page}`, {
                method: 'GET',
                headers: headers
            }).then(res => res.json())
                
            if(data.status.toLowerCase() === 'success') {
                setMessages(data.data.data);
                setTotalData(data.data.totalData);
                setTotalPage(data.data.totalPage);
            } else {
                console.error(data);
                throw new Error(data.message || 'unknown error!');
            }   
        } catch (error) {
            // @ts-ignore
            console.error(error.message || error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        if(messages.length <= 0) {
            loadMessages();
        }
    }, [messages])

    const handleChange = async (event: ChangeEvent<unknown>, value: number) => {

        await loadMessages(value);
        setPage(value);
    }

    return (
        <>  
            <LoadingBackDrop isOpen={loading} />
            <Box style={{ backgroundColor: 'white', padding: '3%', border: '1px white solid', borderRadius: '10px' }}>
                <Button variant='contained' onClick={() => { router.push('/create') }}>
                    Kirim Pesan
                </Button>
                <Typography style={{marginTop: '10px'}}>Pesan Bulan Ini</Typography>
                <Box sx={{ margin: '10px' }}>
                    <Grid container spacing={1}>
                        {
                            messages.length > 0 ? (
                                messages.map((item, index) => {
                                    let isOdd = (index + 1) % 2 === 0 ? false : true;
                                    
                                    return (
                                        <Grid item xs={12} sm={12} md={3.8} key={item._id} style={{margin: '10px'}}>
                                            <BoardCard from={item.from} to={item.to} message={item.message} isOdd={isOdd} />
                                        </Grid>
                                    )
                                })
                            ) : ('')
                        }
                    </Grid>
                </Box>
                <Box sx={{ margin: '10px' }}>
                    <Pagination style={{  }} count={totalPage} page={page} onChange={handleChange} />
                </Box>
            </Box>
        </>
    )
}

ArchiveIndex.getLayout = function getLayout(page: ReactElement) {
    return (
      <BaseLayout>{page}</BaseLayout>
    )
}

export default ArchiveIndex
