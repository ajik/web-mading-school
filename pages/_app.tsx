import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { NextPage } from 'next'
import { ReactElement, ReactNode } from 'react'
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
    const getLayout = Component.getLayout ?? ((page) => page)
    return getLayout(
        <GoogleReCaptchaProvider
            reCaptchaKey={ process.env.PUBLIC_NEXT_RECAPTCHA_SITE_KEY || '' }
            scriptProps={{
                async: false,
                defer: false,
                appendTo: "head",
                nonce: undefined,
            }}
        >
            <Component {...pageProps} />
        </GoogleReCaptchaProvider>
    )
}

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout
}

export type NextPageWithLayout = NextPage & {
    getLayout?: (page: ReactElement) => ReactNode
}

export default MyApp

