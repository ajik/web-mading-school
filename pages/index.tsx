import { ReactElement, useEffect, useState } from 'react'
import BaseLayout from '../components/layout/Base'
import {Typography} from '@mui/material'
// import constants from '../../utils/constants'
import { Box, Grid, Button, Pagination } from '@mui/material'
import BoardCard from '../components/board/board-card'
import { Message } from '../interface/message'
import { useRouter } from 'next/router'
import { NextPageWithLayout } from './_app'

const Home: NextPageWithLayout = () => {
    const [ messages, setMessages ] = useState<Message[]>([]);
    const limit = 15
    const router = useRouter()

    const headers: HeadersInit = new Headers();
    headers.set('Accept', 'application/json');

    useEffect(() => {
        if(messages.length <= 0) {
            fetch(
                `${process.env.PUBLIC_NEXT_BACKEND_API}/board/latest?limit=${limit}`, {
                method: 'GET',
                headers: headers
            }).then(res => res.json())
                .then(data => {
                    if(data.status.toLowerCase() === 'success') {
                        setMessages(data.data);
                    } else {
                        console.error(data);
                        throw new Error(data.message || 'unknown error!')
                    }
                }).catch((e) => {
                    console.error(e.message || e)
                })
        }
    }, [messages])

    return (
        <>  
            <Box style={{ backgroundColor: 'black', padding: '3%', border: '1px white solid', borderRadius: '10px' }}>
                <Button variant='contained' onClick={() => { router.push('/create') }}>
                    Kirim Pesan
                </Button>
                <Typography style={{marginTop: '10px'}}>Pesan Bulan Ini</Typography>
                <Box sx={{ margin: '10px' }}>
                    <Grid container spacing={1}>
                        {
                            messages.length > 0 ? (
                                messages.map((item, index) => {
                                    let isOdd = (index + 1) % 2 === 0 ? false : true;
                                    
                                    return (
                                        <Grid item xs={12} sm={12} md={3.8} key={item._id} style={{margin: '10px'}}>
                                            <BoardCard from={item.from} to={item.to} message={item.message} isOdd={isOdd} />
                                        </Grid>
                                    )
                                })
                            ) : ('')
                        }
                    </Grid>
                </Box>
            </Box>
        </>
    )
}

Home.getLayout = function getLayout(page: ReactElement) {
    return (
        <BaseLayout>{page}</BaseLayout>
    )
}

export default Home
