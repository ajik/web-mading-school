import { ChangeEvent, ReactElement, useEffect, useState } from 'react'
import BaseLayout from '../../components/layout/Base'
import { NextPageWithLayout } from '../_app'
import { Box, Typography, Link as A } from '@mui/material'

const credits = [{
    name: 'Stars_Timelapse_from_Beach_1Videvo_preview',
    assetUrl: 'https://cdn.videvo.net/videvo_files/video/free/2014-01/large_watermarked/Stars_Timelapse_from_Beach_1Videvo_preview.mp4',
    tags: [ 'video', 'background' ],
    provider: 'videvo',
    category: 'video',
    url: 'https://www.videvo.net/'
}]

const AboutMe: NextPageWithLayout = () => {

    return (
        <>  
            <Box style={{ color: 'black', backgroundColor: 'white', padding: '3%', border: '1px white solid', borderRadius: '10px' }}>
                <Typography variant='h4'>Tentang Web Ini & Saya</Typography>
                <Typography  component={'div'} variant='body1' paragraph textAlign={'justify'}>
                    Hello, nama saya ~~~, saat ini software engineer yang sedang idle (a.k.a software engineer pengangguran). 
                    Project ini adalah project sederhana yang ditujukan untuk merefleksikan sebuah majalah dinding (mading) sekolah. 
                    Mading sekolah adalah majalah dinding yang digunakan untuk berkirim pesan.
                    Terinspirasi mading sekolah saat SMA yang biasanya digunakan untuk berkirim pesan kepada teman, gebetan, pacar, guru, anonim (pemuja rahasia ceritanya *langsung setel lagunya SO7 pemuja rahasia) atau hal-hal lain.
                    Situs ini dibuat dengan tujuan itu, saya hanya menambahkan fitur google recaptcha untuk mencegah DDOS
                    karena teknologi yang dipakai bersifat open source dan tidak berbayar.
                    <br />
                    (Note: Namun jika para pembaca ingin menyisihkan sebagian kecil receh untuk saya maka saya tidak akan keberatan untuk menerimanya :&apos;, Saya mungkin menambahkan beberapa fitur donasi nantinya).
                    <br />
                    Dics. Mohon maaf kalau pertama kali load atau muat ulang secara paksa akan memakan waktu sedikit lama karena latar belakangnya menggunakan Mp4 video *Teehee~!
                </Typography>
                <Typography variant='body1' paragraph textAlign={'justify'}>
                    Seperti yang sudah saya sebutkan sebelumnya, bahwa ini hanya project sampingan. Tidak menggunakan teknologi yang sulit.
                    Untuk UI menggunakan teknologi berbasis javascript dengan menggunakan nextjs dan menggunakan recaptcha untuk mencegah database menerima DDOS dan server menggunakan NestJS dan Mongo database. 
                    Semua tampilan berlatar bahasa Indonesia dan tidak menyematkan fitur translasi ke bahasa inggris.
                </Typography>
                <Typography variant='body1' paragraph textAlign={'justify'}>
                    Jika para pembaca ingin melihat kode pada program ini, bisa membuka laman ini.<br />
                    FrontEnd: <A underline="hover" target={'_blank'} href={'https://gitlab.com/ajik/web-mading-school'}>Klik disini untuk melihat kode source FrontEnd</A><br />
                    BackEnd: <A underline="hover" target={'_blank'} href={'https://gitlab.com/ajik/api-mading-school'}>Klik disini untuk melihat kode source BackEnd</A>
                </Typography>
                <Typography variant='h4'>Kredit</Typography>
                <Typography component={'div'}  variant='body1' paragraph textAlign={'justify'}>
                    {
                        credits.map(item => {
                            return (
                                <p key={ item.name }>
                                    <strong>Kategori: { item.category }</strong><br />
                                    Nama: { item.name }<br />
                                    Tags: {
                                        item.tags.length > 0 ? (
                                            item.tags.map(tag => <span key={tag}>{`#${tag}`}&nbsp;&nbsp;&nbsp;</span>)
                                        ) : ''
                                    }<br />
                                    Provider: <A underline="hover" target={'_blank'} href={ item.url }>{ item.provider }</A><br />
                                    AssetUrl: <A underline="hover" target={'_blank'} href={ item.assetUrl }>{ item.name }</A>
                                </p>
                            )
                        })
                    }
                </Typography>
            </Box>
        </>
    )
}

AboutMe.getLayout = function getLayout(page: ReactElement) {
    return (
      <BaseLayout>{page}</BaseLayout>
    )
}

export default AboutMe
