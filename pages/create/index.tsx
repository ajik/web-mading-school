import { FormEvent, ReactElement, useState } from 'react'
import BaseLayout from '../../components/layout/Base'
import { NextPageWithLayout } from '../_app'
import { Typography } from '@mui/material'
// import constants from '../../utils/constants'
import { Box, Grid, Button, TextField } from '@mui/material'
import dynamic from 'next/dynamic'

const ReactQuill = dynamic(() => import('react-quill'), { ssr: false })
import 'react-quill/dist/quill.snow.css';
import Notification from '../../components/notification'
import { useRouter } from 'next/router'
import { useGoogleReCaptcha } from "react-google-recaptcha-v3"

const CreateNewMessage: NextPageWithLayout = () => {

    const [ message, setMessage ] = useState('')
    const [ from, setFrom ] = useState('')
    const [ to, setTo ] = useState('')
    const [ email, setEmail ] = useState('')
    const [ notificationOpen, setNotificationOpen ] = useState(false) 
    const [ notificationMessage, setNotificationMessage ] = useState('')
    const router = useRouter()
    const { executeRecaptcha } = useGoogleReCaptcha();

    const messageToolbar = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
        ]
    }

    const submitMessage = async (e: FormEvent) => {
        e.preventDefault();
        let requestBody = {from, to, email, message}
        console.log(requestBody);
        let headers: HeadersInit = new Headers();
        headers.set('Accept', 'application/json')
        headers.set('Content-Type', 'application/json')
        // tinggal di submit
        // alert('oit')
        try {
            if (!executeRecaptcha) {
                console.error("Execute recaptcha not yet available");
                return;
            }

            // do captcha
            const gReCaptchaToken = await executeRecaptcha(process.env.PUBLIC_NEXT_RECAPTCHA_ACTION || 'NotFoundCaptcha');
            console.log(gReCaptchaToken, "response Google reCaptcha server");

            // @ts-ignore
            requestBody.captchaToken = gReCaptchaToken;

            // console.info(requestBody);

            const data = await fetch(`${process.env.PUBLIC_NEXT_BACKEND_API}/board`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(requestBody)
            }).then(res => res.json());

            console.info(data);

            if(data.status === 'success') {
                setNotificationMessage('Pesan kamu tersimpan!')
                setNotificationOpen(true)

                setTimeout(()=>{
                    router.push('/board')
                }, 3500)
            } else {
                throw new Error(data.message)
            }

            

        } catch (error) {
            // @ts-ignore
            setNotificationMessage(`Error: ${ error.message || error }`)
            setNotificationOpen(true)
        }
    }

    const closeNotification = () => {
        setNotificationOpen(false)
    }
    
    return (
        <>
            <Notification open={notificationOpen} message={notificationMessage} onClose={closeNotification} />
            <Box style={{ color: 'black', backgroundColor: 'white', padding: '3%', border: '1px white solid', borderRadius: '10px' }}>

                <Typography>Tambah Pesan Baru</Typography>
                <form onSubmit={submitMessage}>
                    <Grid container spacing={2} style={{marginTop: '10px'}}>
                        <Grid item xs={12} style={{maxWidth: '600px'}}>
                            <TextField
                                required
                                id="from"
                                label="Nama Kamu"
                                style={{
                                    maxWidth: '800px',
                                    minWidth: '300px'
                                }}
                                value={from}
                                fullWidth
                                onChange={(e) => setFrom(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12} style={{maxWidth: '600px'}}>
                            <TextField
                                required
                                id="to"
                                label="Penerima Pesan"
                                fullWidth
                                value={to}
                                onChange={(e) => setTo(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12} style={{maxWidth: '600px'}}>
                            <TextField
                                required
                                id="email"
                                label="Email kamu"
                                helperText="* Email hanya digunakan untuk mengirim copy pesan kamu"
                                fullWidth
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}  style={{maxWidth: '600px'}}>
                            <ReactQuill theme="snow" style={{minHeight: '100px'}} modules={messageToolbar} placeholder={'Masukkan pesan kamu'} value={message} onChange={setMessage} />
                        </Grid>
                        <Grid item xs={12}  style={{maxWidth: '600px'}} textAlign="right">
                            <Button type='submit' variant='contained'>
                                Submit
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        </>
    )
}

CreateNewMessage.getLayout = function getLayout(page: ReactElement) {
    return (
      <BaseLayout>{page}</BaseLayout>
    )
}

export default CreateNewMessage
