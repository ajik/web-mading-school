
import { ReactElement } from 'react'
import BaseLayout from '../components/layout/Base'
import { NextPageWithLayout } from './_app'
import {Typography} from '@mui/material'
import constants from '../utils/constants'

const ProtoTypingLayout: NextPageWithLayout = () => {
    return (
        <Typography>
            {constants.LoremIpsum[0]}
        </Typography>   
    )
}

ProtoTypingLayout.getLayout = function getLayout(page: ReactElement) {
    return (
      <BaseLayout>{page}</BaseLayout>
    )
}

export default ProtoTypingLayout
