/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    env: {
        PUBLIC_NEXT_BACKEND_API: process.env.PUBLIC_NEXT_BACKEND_API,
        PUBLIC_NEXT_RECAPTCHA_SITE_KEY: process.env.PUBLIC_NEXT_RECAPTCHA_SITE_KEY,
        PUBLIC_NEXT_RECAPTCHA_ACTION: process.env.PUBLIC_NEXT_RECAPTCHA_ACTION,
        PUBLIC_NEXT_APP_VERSION: process.env.PUBLIC_NEXT_APP_VERSION
    }
}

module.exports = nextConfig
