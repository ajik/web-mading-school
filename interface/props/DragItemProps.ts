export default interface DragItemProps {
    type: string
    id: string
    top: number
    left: number
}
  