export type Message = {
    _id: any; // maapkan masih pakai any :')
    message: string;
    from: string;
    to: string;
    month: string;
    year: string;
    epochTime: number;
}